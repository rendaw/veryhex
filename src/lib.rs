use std::fmt::{Binary, Debug, Display};
use std::mem::size_of_val;

use ieee754::Ieee754;
use num::traits::NumAssignOps;
use num::{CheckedMul, PrimInt, Zero};
use std::ops::{BitOrAssign, BitXorAssign, ShlAssign};

#[derive(Debug)]
enum Et {
    BadCharacter,
    TooWide,
    TooBig,
    TooSmall,
}

trait FromPrimitiveAs<T> {
    fn from_(x: T) -> Self;
}

impl FromPrimitiveAs<u8> for u32 {
    fn from_(x: u8) -> Self {
        x as u32
    }
}

impl FromPrimitiveAs<usize> for u32 {
    fn from_(x: usize) -> Self {
        x as u32
    }
}

impl FromPrimitiveAs<u8> for u64 {
    fn from_(x: u8) -> Self {
        x as u64
    }
}

impl FromPrimitiveAs<usize> for u64 {
    fn from_(x: usize) -> Self {
        x as u64
    }
}

impl FromPrimitiveAs<i32> for u8 {
    fn from_(x: i32) -> Self {
        x as u8
    }
}

impl FromPrimitiveAs<i32> for u16 {
    fn from_(x: i32) -> Self {
        x as u16
    }
}

#[inline]
fn parse_hexit<S>(b: u8) -> S
where
    S: FromPrimitiveAs<u8>,
{
    return match b {
        b'0'..=b'9' => <S as FromPrimitiveAs<u8>>::from_(b - b'0'),
        b'a'..=b'f' => <S as FromPrimitiveAs<u8>>::from_(b - b'a' + 10),
        _ => panic!("dead code"),
    };
}

fn read_remaining_zero(raw: &[u8], mut i: usize, mut exp: i32) -> Result<usize, Et> {
    let mut read_bits: usize = 0;
    loop {
        if i >= raw.len() {
            return Ok(read_bits);
        }
        if exp >= 126 {
            return Err(Et::TooBig);
        }
        let b = raw[i];
        match b {
            b'.' => {
                i += 1;
                break;
            }
            b'0' => {}
            b'1'..=b'9' | b'a'..=b'f' => {
                return Err(Et::TooWide);
            }
            _ => {
                return Err(Et::BadCharacter);
            }
        }
        i += 1;
        read_bits += 4;
        exp += 4;
    }
    read_remaining_zero_fract(raw, i)?;
    return Ok(read_bits);
}

fn read_remaining_zero_fract(raw: &[u8], i: usize) -> Result<(), Et> {
    for b in &raw[i..] {
        match b {
            b'.' => {
                break;
            }
            b'0' => {}
            b'1'..=b'9' | b'a'..=b'f' => {
                return Err(Et::TooWide);
            }
            _ => {
                return Err(Et::BadCharacter);
            }
        }
    }
    Ok(())
}

fn parse_raw_inner<S, E>(raw: &[u8], sig_bits: usize) -> Result<(bool, E, S), Et>
where
    S: PrimInt
        + NumAssignOps
        + BitXorAssign
        + BitOrAssign
        + ShlAssign
        + FromPrimitiveAs<u8>
        + FromPrimitiveAs<usize>
        + Display
        + CheckedMul
        + Zero
        + Binary,
    E: Debug + FromPrimitiveAs<i32> + Zero,
{
    if raw.len() == 0 {
        return Ok((false, Zero::zero(), Zero::zero()));
    }

    let mut i: usize = 0;
    let mut int: S = Zero::zero();
    let mut exp: i32 = 0;
    let mut read_bits: usize = 0;

    // Parse sign
    let mut neg = false;
    if raw[0] == b'-' {
        neg = true;
        i += 1;
    }
    // Read integer up to first hexit
    'int: loop {
        break {
            loop {
                if i >= raw.len() {
                    return Ok((neg, Zero::zero(), Zero::zero()));
                }
                let b = raw[i];
                match b {
                    b'.' => {
                        break 'int;
                    }
                    b'0' => {}
                    b'1'..=b'9' | b'a'..=b'f' => {
                        int = parse_hexit(raw[i]);
                        read_bits = size_of_val(&int) * 8 - int.leading_zeros() as usize;
                        exp = read_bits as i32;
                        read_bits -= 1;
                        int ^= <S as FromPrimitiveAs<usize>>::from_(1 << read_bits);
                        int <<= <S as FromPrimitiveAs<usize>>::from_(sig_bits - read_bits);
                        i += 1;
                        break;
                    }
                    _ => {
                        return Err(Et::BadCharacter);
                    }
                }
                i += 1;
            }

            // Read remaining integer digits
            loop {
                if i >= raw.len() {
                    break;
                }
                let b = raw[i];
                match b {
                    b'.' => {
                        break 'int;
                    }
                    b'0'..=b'9' | b'a'..=b'f' => {
                        if read_bits + 4 > sig_bits {
                            exp = read_remaining_zero(raw, i, exp)? as i32;
                            return Ok((neg, <E as FromPrimitiveAs<i32>>::from_(exp + 126), int));
                        }
                        int |= parse_hexit::<S>(b) << (sig_bits - read_bits);
                        exp += 4;
                        read_bits += 4;
                    }
                    _ => {
                        return Err(Et::BadCharacter);
                    }
                }
                i += 1;
            }
        };
    }
    i += 1; // move past .
    if exp == 0 {
        // Hasn't seen a non-0 digit yet, read until first non-zero
        loop {
            if i >= raw.len() {
                return Ok((neg, Zero::zero(), Zero::zero()));
            }
            let b = raw[i];
            match b {
                b'0' => {
                    exp -= 4;
                }
                b'1'..=b'9' | b'a'..=b'f' => {
                    int = parse_hexit(raw[i]);
                    read_bits = size_of_val(&int) * 8 - int.leading_zeros() as usize;
                    exp -= 4 - read_bits as i32;
                    read_bits -= 1;
                    int ^= <S as FromPrimitiveAs<usize>>::from_(1 << read_bits);
                    int <<= <S as FromPrimitiveAs<usize>>::from_(sig_bits - read_bits);
                    break;
                }
                _ => {
                    return Err(Et::BadCharacter);
                }
            }
            if exp <= -126 {
                return Err(Et::TooSmall);
            }
            i += 1;
        }
        i += 1; // move past first digit
    }

    // Read remaining fractional places
    loop {
        if i >= raw.len() {
            break;
        }
        let b = raw[i];
        match b {
            b'0' => {
                read_bits += 4;
            }
            b'1'..=b'9' | b'a'..=b'f' => {
                if read_bits + 4 > sig_bits {
                    read_remaining_zero_fract(raw, i)?;
                    return Ok((neg, <E as FromPrimitiveAs<i32>>::from_(exp + 126), int));
                }
                read_bits += 4;
                let temp = parse_hexit::<S>(raw[i]);
                int ^= temp << (sig_bits - read_bits);
            }
            _ => {
                return Err(Et::BadCharacter);
            }
        }
        i += 1;
    }

    return Ok((neg, <E as FromPrimitiveAs<i32>>::from_(exp + 126), int));
}

#[cfg(test)]
mod tests {
    use ieee754::Ieee754;

    use crate::parse_raw_inner;

    #[test]
    fn test_parse_raw_inner() {
        for fix in vec![
            ("", (false, 0u8, 0u32)),
            ("0", (false, 0u8, 0u32)),
            ("0.", (false, 0u8, 0u32)),
            (".", (false, 0u8, 0u32)),
            (".0", (false, 0u8, 0u32)),
            ("-0", (true, 0u8, 0u32)),
            ("-", (true, 0u8, 0u32)),
            ("1", (false, 127u8, 0u32)),
            ("2", (false, 128u8, 0u32)),
            ("3", 3f32.decompose_raw()),
            ("3", (false, 128u8, 0x400000u32)),
            ("4", (false, 129u8, 0u32)),
            ("0.8", (false, 126u8, 0u32)),
            ("0.80", (false, 126u8, 0u32)),
            ("0.4", ((1.0 / 4.0) as f32).decompose_raw()),
            ("0.4", (false, 125u8, 0u32)),
            ("0.a", ((5.0 / 8.0) as f32).decompose_raw()),
            ("0.a", (false, 126u8, 0x200000u32)),
            ("1.8", (false, 127u8, 0x400000u32)),
            ("1.88", (false, 127u8, 0x440000u32)),
            ("11.88", (false, 131u8, 0x844000u32)),
            ("10000000000000000000000000000000", (false, 230u8, 0u32)),
            (".0000000000000000000000000000001", (false, 3u8, 0u32)),
        ] {
            println!("Test [{}]", fix.0);
            let res = parse_raw_inner::<u32, u8>(fix.0.as_bytes(), 23);
            match res {
                Ok(x) => {
                    assert_eq!(x, fix.1);
                    println!(
                        "As f32: {}",
                        f32::recompose_raw(fix.1 .0, fix.1 .1, fix.1 .2)
                    )
                }
                Err(x) => assert!(false, "err = {:?}", x),
            }
        }
    }
}

trait Mixedecimal<T> {
    fn parse_mixedecimal(raw: &[u8]) -> Result<T, Et>;
}

impl Mixedecimal<f32> for f32 {
    fn parse_mixedecimal(raw: &[u8]) -> Result<Self, Et> {
        let (sign, exp, sig) = parse_raw_inner(raw, 23)?;
        Ok(Ieee754::recompose_raw(sign, exp, sig))
    }
}

impl Mixedecimal<f64> for f64 {
    fn parse_mixedecimal(raw: &[u8]) -> Result<Self, Et> {
        let (sign, exp, sig) = parse_raw_inner(raw, 52)?;
        Ok(Ieee754::recompose_raw(sign, exp, sig))
    }
}
